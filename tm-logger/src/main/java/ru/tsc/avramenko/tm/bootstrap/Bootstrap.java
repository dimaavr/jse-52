package ru.tsc.avramenko.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.api.ILoggingService;
import ru.tsc.avramenko.tm.api.IPropertyService;
import ru.tsc.avramenko.tm.service.LoggingService;
import ru.tsc.avramenko.tm.service.PropertyService;

import javax.jms.JMSException;
import java.util.List;

public final class Bootstrap {

    public void run() {
        @NotNull final ILoggingService loggingService = new LoggingService();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final List<String> entities = propertyService.getLogEntities();
        entities.forEach(item -> {
            try {
                loggingService.createBroadcastConsumer(item);
            } catch (JMSException e) {
                e.fillInStackTrace();
            }
        });
    }

}