package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change the user's password.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER ID: ");
        @Nullable final String userId = TerminalUtil.nextLine();
        @NotNull final String currentUserId = serviceLocator.getSessionService().getSession().getUserId();
        if (!userId.equals(currentUserId)) throw new AccessDeniedException();
        @NotNull final boolean userExists = serviceLocator.getUserEndpoint().existsUserById(session, userId);
        if (!userExists) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().updateUserPassword(session, password);
        serviceLocator.getCommandService().getCommandByName("logout").execute();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}