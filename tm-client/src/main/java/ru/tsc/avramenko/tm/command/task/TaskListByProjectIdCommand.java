package ru.tsc.avramenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.List;
import java.util.Optional;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Display task list by project id.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = serviceLocator.getProjectEndpoint().findProjectById(session, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findTaskByProjectId(session, projectId);
        if (tasks.size() == 0) throw new TaskNotFoundException();
        int index = 1;
        for (TaskDTO task : tasks) {
            System.out.println("\n" + "|--- Task [" + index + "]---|");
            showTask(task);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}